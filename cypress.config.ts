import { defineConfig } from "cypress";
import * as dotenv from 'dotenv'

dotenv.config()

export default defineConfig({
  env: {
    auth0_username: process.env.EXTERNAL_USER,
    auth0_password: process.env.EXTERNAL_PASSWORD,
    auth0_domain: process.env.AUTH0_DOMAIN,
    auth0_scope: process.env.AUTH0_SCOPE,
    auth0_client_id: process.env.AUTH0_CLIENT_ID,
    auth0_client_secret: process.env.AUTH0_CLIENT_SECRET,
    base_url: `${process.env.SERVER_URL}:${process.env.SERVER_PORT}/`
  },
  e2e: {
    baseUrl: `${process.env.SERVER_URL}:${process.env.SERVER_PORT}`
  },
});
