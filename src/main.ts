import express, { NextFunction, Request, Response } from 'express';
import next from 'next';
import cors from 'cors';
import { auth, requiresAuth } from 'express-openid-connect';

const dev = process.env.NODE_ENV !== 'production'

const refreshToken = async (req: Request, res: Response, next: NextFunction) => {
    if (!req.oidc.accessToken) return res.redirect("/login");

    // Refresh token logic
    let { expires_in, isExpired, refresh } = req.oidc.accessToken;
    console.log("expires_in", expires_in, "isExpired", isExpired());
    
    if (isExpired()) {
        await refresh()
    }
    next();
}

const checkAuthorization = (role: string) => {
    return (req: Request, res: Response, next: NextFunction) => {
        const roles = req.oidc.idTokenClaims?.[process.env.AUTH0_CLAIM_NAMESPACE ?? ''] as string[] ?? [];
        if (roles.includes(role)) return next();

        // Redirects to an error page
        res.redirect('/error/401')
    }
}

async function main () {
    try {
        const nextApp = next({ dev })
        const handler = nextApp.getRequestHandler()
        await nextApp.prepare()

        const server = express()
        server.use(cors())

        // Auth routes are provided by the auth0 library
        server.use(auth({
            authRequired: false,
            idpLogout: true, // allows to logout using the /logout route provided by the lib
            baseURL: `${process.env.SERVER_URL}:${process.env.SERVER_PORT}`,
            clientID: process.env.AUTH0_CLIENT_ID,
            clientSecret: process.env.AUTH0_CLIENT_SECRET,
            issuerBaseURL: `https://${process.env.AUTH0_DOMAIN}`,
            secret: process.env.AUTH0_SECRET,
            authorizationParams: {
                response_type: 'code',
                response_mode: 'form_post',
                scope: process.env.AUTH0_SCOPE
            },
            afterCallback: (req, res, session, state) => {  
                return session;
            },
        }));

        // Protected routes
        server.get('/', [refreshToken, requiresAuth()], async (req: Request, res: Response) => {
            return nextApp.render(req, res, req.path)
        })

        server.get('/internal/manager', [refreshToken, checkAuthorization('manager')], async (req: Request, res: Response) => {
            return nextApp.render(req, res, req.path)
        })

        // Public routes
        server.get('/about', (req, res) => {
            return nextApp.render(req, res, req.path)
        })
        
        server.get('/_next/*', async (req: Request, res: Response) => {
            return handler(req, res)
        })

        server.get('(.*)', async (req: Request, res: Response) => {
            return handler(req, res)
        })

        server.get('/error/401', async (req: Request, res: Response) => {
            return nextApp.render(req, res, '/errors/401')
        })

        // Error handling
        server.use(async (err: Error, req: Request, res: Response, next: NextFunction) => {
            return nextApp.render(req, res, '/_error')
        })

        server.listen(process.env.SERVER_PORT, () => {
            console.info(`Server running at ${process.env.SERVER_URL}:${process.env.SERVER_PORT}`)
        })
        
        console.info(`Visit App at ${process.env.CLIENT_URL}:${process.env.CLIENT_PORT}`)
    } catch (err) {
        const error = err as Error
        console.error(error)
        process.exit(1)
    }
}

export default main()
