import '@/styles/globals.css'
import { RequestContext } from 'express-openid-connect'
import { IncomingMessage } from 'http'
import type { AppContext, AppProps } from 'next/app'

interface IncomingMessageWithUser extends IncomingMessage {
  oidc: RequestContext
}

type AdditionalProps = {
  oidc?: RequestContext & { roles: string[] }
  user?: Record<string, unknown>
}

type PageProps = AppContext & AdditionalProps | AdditionalProps

export default function App({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />
}

App.getInitialProps = async ({ Component, ctx }: AppContext) => {
  const { req, res, asPath } = ctx

  const pageProps: PageProps = Component.getInitialProps
        ? await Component.getInitialProps(ctx) ?? {}
        : {}

  if (!req) return { pageProps };

  const oidc = (req as IncomingMessageWithUser).oidc;
  pageProps.user = oidc.user;
  
  // Add roles array to the oidc object that is injected into each page
  const roles = oidc.idTokenClaims?.[process.env.AUTH0_CLAIM_NAMESPACE ?? ''] as string[] ?? [];
  pageProps.oidc = { ...oidc, roles: roles };

  return { pageProps }
};