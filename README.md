## Experiment with NextJS + Express + Auth0

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

Open [http://localhost:3030](http://localhost:3030) with your browser to see the result.

### E2E tests

1. `yarn dev`
2. `yarn e2e`