/// <reference types="cypress" />

declare namespace Cypress {
    export interface Chainable {
        loginToAuth0 (username: string, password: string): Chainable<void>
    }
}
