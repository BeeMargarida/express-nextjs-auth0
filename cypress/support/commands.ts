/// <reference types="cypress" />

function loginViaAuth0Ui(username: string, password: string) {
  // Login on Auth0
  cy.origin(
    Cypress.env("auth0_domain"),
    { args: { username, password } },
    ({ username, password }) => {
      cy.visit(Cypress.env("base_url"));
      cy.get("input#username").type(username);
      cy.get("#password").type(`${password}{enter}`, { log: false });
    }
  );

  // Ensure Auth0 has redirected us back to the App
  cy.url().should("equal", Cypress.env("base_url"));
}

Cypress.Commands.add("loginToAuth0", (username: string, password: string) => {
  const log = Cypress.log({
    displayName: "AUTH0 LOGIN",
    message: [`🔐 Authenticating | ${username}`],
    // @ts-ignore
    autoEnd: false,
  });
  log.snapshot("before");

  loginViaAuth0Ui(username, password);

  log.snapshot("after");
  log.end();
});
